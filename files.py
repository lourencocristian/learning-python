# Managing files

# Read a file
f = open('names.txt')

# print(f.read()) # read all lines in the open file
# print(f.readline()) # read one line

# read line by line
for x in f:
    print(x)

f.close() # close file

# Adding lines with append
f = open('names.txt', 'a') # open file with append permision

f.write('\n Writing new line') # add new line. \n use for return new line like ENTER
f.write('\n Writing new line')
f.write('\n Writing new line')

f.close()

# Writing
f = open('names.txt', 'w') # open file with append permision
f.write('\n Writing new line')
f.close()

f = open('names.txt', 'r') 
f.read()

print(f.read())

# Removing files
import os # import operative system module 

if os.path.exists('names.txt'):
    os.remove('names.txt')
else:
    print('File does not exist')
