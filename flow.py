# Managin workflow

# if

isAlive = True

if isAlive == True: print("Is Alive") # One line if

# if with else
isAlive = False
if isAlive == True:
    print("Is Alive")
else: 
    print("Is not alive")

# elif
if 5 != 4:
    print("5 is diferent from 4")
elif 5 == 5:
    print("5 is equals to 5")
else:
    print("Nothing to do")

print("isAlive: " + str(isAlive))
if isAlive == False and 5 == 5: print("All is true")
if isAlive != False or 5 == 5: print("All is true")