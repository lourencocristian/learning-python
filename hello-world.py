# First line of code with Python
print('Hello world!')

# Check indentation
if 5 >= 3:
    print('5 es mayor o igual a 3')

# Variables
name = 'Cristian Gabriel Lourenco'
email = 'lourencocristian@gmail.com'
age = 35

print('Name: ' + name)
print('Email: ' + email)
print('Age: ' + str(age))  # Convert int to string with str function

# Lists

list = [1, 2, 3]
list2 = list.copy()
list.append(4)

print(list)

list.clear()

print(list, list.count(2))  # Count
print(list2, len(list2))  # Print size
print(list2[0])  # Get first element in the list2

# list2.pop() # Remove last element
# list2.remove(1) # Remove specific element

list2.reverse()
print(list2)

list2.sort()
print(list2)

# Tuples

tuple = ('Hola', 'mundo')
tuple = (list2)

# Ranges

range = range(10)
print(range)

# Dictionary

dictionary = {
    "name": 'Cristian Gabriel Lourenco',
    "email": 'lourencocristian@gmail.com',
    "age": 35
}
print(dictionary)
print(dictionary["name"])  # Specific value from dictionary
print(dictionary.get("email"))  # Specific value from dictionary

dictionary['age'] = 35+1
print(dictionary.get('age'))

dictionary['sex'] = "M"
print(dictionary)

dictionary.popitem()  # Remove last item
print(dictionary)

dictionary.clear()  # Remove all values
print(dictionary)

# More complex dictionary
people = {
    "Males": {
        "Gabriel": {
            "fullName": 'Cristian Gabriel Lourenco',
            "email": 'lourencocristian@gmail.com',
            "age": 35
        }
    },
    "Females":
    {
        "Monica":
        {
            "fullName": 'Monica Angelica Rodriguez',
            "email": 'monik@gmail.com',
            "age": 39

        },
        "Cristina":
        {
            "fullName": 'Cristina Figueroa',
            "email": 'cris@gmail.com',
            "age": 20
        }
    }
}
print(people)

# Boolean

isAlive = True
isSleep = False
print(isAlive, isSleep)
