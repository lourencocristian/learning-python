# Learning Python

Project to play and learn with **Python**. This project follow the [***Python sin fronteras: HTML, CSS, Flask y MySQL***](https://www.udemy.com/course/python-sin-fronteras-html-css-mysql) course from [**Nicolas Schurmann**](https://www.nicolas-schurmann.com/).

## Data types 💾

Learning data types in Python. See: [**hello-world**](hello-world.py) file.

- strings = "" or ''
- ints = 1
- lists = []
- tuple = ()
- dictonary = {} or dict(name="Name", age=44)

## Flow ↪️ ↩️

Learning intruccions for flow. See: [**flow**](flow.py) file.

- if, elif, else
- and/or

## Inputs ⌨️

Learning data inputs and using operators. See: [**excercise**](excercise.py) file.

Try to use the simple calculator.

## Loops 🔃

Learning loops. See: [**loops**](loops.py) file.

- while
- for

## Functions

Play with functions. See [**functions**](functions.py) file.

- Variables and arguments
- Defaults
- Return
- Recursive

## Objects

Learning about clases, instance of a class, methods, inheritance. See [**objects**](objects.py) file.

- Clases
- Objects
- Methods
- Inheritance

## Modules

Using modules.  See [**modules**](modules.py) file.

- import
- from "module" import
- rename with "as" 

## HTML & CSS </> {}

Learning HTML. See [**HTML README**](html/README.md)

## Database

Learning how to use a database with Python. See [**Database README**](database/README.md)

## Flask

Using Flask framework. See [**Flask README**](intro-flask/README.md)

## Jupyter Notebooks

Using **Jupyter Notebooks** to practice and test scripts with [**Google Colab**](https://colab.research.google.com/drive/1F8yMIXN6Y5WicfYAK8dtNsgB3omlLfDd?usp=sharing)

## TkInter 💬️

User interface application. See [**TkInter README**](TkInter/README.md)
