# HTML

Folder to play and learn **HTML and CSS**. This site is publish with [**Gitlab Pages**](https://docs.gitlab.com/ee/user/project/pages/) for demo, you can see in: https://lourencocristian.gitlab.io/learning-python

## HTML </>

Learning basic HTML. See: [**index.css**](html/index.css)

- Head and sintax
- Body and basic labels
- Titles and paragraphs
- Format
- Forms
- Imputs
- Images
- Links
- Lists
- Tables
- Div's 

## Using CSS {}

Learning css for our index.html. See: [**main.css**](html/main.css).

- Body
- Form
- Images
- Table
- Div's
- Paragraphs
- Links
- Lists
- Display
- Position
- Overflow
- Float
- Navigation bar
- Dropdown
- Inputs
