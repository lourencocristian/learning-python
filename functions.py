# Functions

def myFunction():
    print('My first function')

myFunction()

# Function with arguments
def mySeccondFunction(firstArg):
    print('My first function is', firstArg)

mySeccondFunction('cats')

# Function with 2 arguments
def fullName(name, lastName):
    print('My fullname is:', name, lastName)

fullName('Gabriel', 'Lourenco')

# Fuction with variable arguments. 
def fullName2(*name):
    print('My fullname is:', name) # With * we define a tuple

fullName2('Crisitan', 'Gabriel', 'Lourenco')

# Calling function with explicit variable name
def fullName3(lastName, name):
    print('My fullname is built with variables is:', name, lastName)

fullName3(name='Gabriel', lastName='Lourenco')

# Function with kwargs
def fullName4(**kwargs):
    print('My fullname built with kwargs is:', kwargs['name'], kwargs['lastName'])

fullName4(name='Gabriel', lastName='Lourenco')

# Function with defaults
def fullName5(name = 'Gabriel Lourenco'):
    print('My fullname built with defaults is:', name)

fullName5()

# Calling function with return instruccion
def fullName6(list):
    i = ''
    for el in list:
        i = i + el + ' '
        return i

fullName6(['Gabriel', 'Lourenco']) # Not execute nothing because we need a variable to fill with de return
name = fullName6(['Gabriel', 'Lourenco']) 

print(name)

# 
def recursive(i):
    if(i < 1):
        return i
    print(i)
    recursive(i - 1)

recursive(6)    