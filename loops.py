# Loops

# While
i = 0

while i <= 500000:
    print(i)
    if i == 10:
        break
    i += 1

# For

names = ("Gabriel", "Roberto", "Carlos", "Jesus", "Angela", "Monica")

for user in names:
    if user == "Jesus":
        break
    print(user)

for user in names:
    if user == "Jesus":
        continue
    print(user)
