from tkinter import *

root = Tk()
root.title('Hello buttons world')
root.geometry('640x480')

def click():
    l = Label(root, text="Hello world")
    l.pack()

btn = Button(root, text="Click-me", command=click, fg='red', bg='black')
btn.pack()

root.mainloop()
