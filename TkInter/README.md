# TkInter

Learning how develop user interface application with **TkInter**.

## Preparing

```bash
sudo apt install python3.8-tk
```

## Simple window

See [**intro.py**](./intro.py)

## Buttons

See [**button.py**](./intro.py)
