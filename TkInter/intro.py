from tkinter import *

root = Tk()
root.title('Hello world')
root.geometry('640x480')

#label = Label(root, text='Hello labels')
#label.pack()

l1 = Label(root, text='Row 1 for grid')
l2 = Label(root, text='Row 2 for grid')

l1.grid(row=0, column=0)
l2.grid(row=1, column=0)

root.mainloop()
