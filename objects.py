# Objects

## Building our first class
class User: 
	name = 'Gabriel'
	lastName = 'Lourenco'

user = User()

#print(user) # you don't print a object
print(user.name, user.lastName)

class User2: 
	def __init__(self, name, lastName):
		self.name = name
		self.lastName = lastName

user2 = User2('Gabriel', 'Lourenco')
user3 = User2('Carolina', 'Lourenco')

print(user2.name, user2.lastName, user3.name, user3.lastName)

# Methods
class User3: 
	def __init__(self, name, lastName):
		self.name = name
		self.lastName = lastName
	def sayHello(self):
		print('Hello I am', self.name, self.lastName)

user3 = User3('Gabriel', 'Lourenco')

user3.sayHello()

#del user3 # Delete an object
print(user3.name)

# Inheritance
class Admin(User3): # The Admin class inherite from User3 class so it can access to all objects in User3 class
	def superHello(self):
		print('Hello my name is', self.name, ' and I am an admin user')

admin = Admin('Gabriel', 'Lourenco')
admin.sayHello()
admin.superHello()
