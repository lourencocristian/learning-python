from flask import Flask, request, url_for
from flask.globals import request

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello world' 

@app.route('/sayhello')
def sayhello():
    print(url_for('index')) # Redirect to index function
    return 'Hello' 

@app.route('/post/<post_id>', methods=['GET', 'POST'])
def post(post_id):
    if request.method == 'GET':
        return 'Post ID is: ' + post_id
    else:
        return 'NOT GET'

