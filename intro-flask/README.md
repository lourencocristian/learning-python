# Flask

Learning basics about [**Flask framework**](https://flask.palletsprojects.com/en/2.0.x/).

## Virtual environment

Create an activate environment

```bash
python3 -m venv venv

. venv/bin/activate
```

## Hello world app

See: [**Hello World file](helloworld.py)

### Setting development environment mode

```bash
export FLASK_ENV=development
```

## Methods

- GET
- POST
- PUT
- PATCH
- DELETE

```bash
curl -X GET http://localhost:5000/post/1
curl -X POST http://localhost:5000/post/1
```

## URL's

