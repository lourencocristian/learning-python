/* Setup a sample database */

CREATE SCHEMA `hello_mysql`;

USE hello_mysql;

CREATE TABLE `hello_mysql`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(254) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);

INSERT INTO hello_mysql.user (id, email)
VALUES (1, 'lourencocristian@gmail.com');

SELECT * FROM hello_mysql.user;
