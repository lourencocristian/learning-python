# Database

Using mysql database with Python. For this section we use mysql environment to play and learn. 

## Mysql local enviroment

To setup local environment follow: [**database.sql**](database.sql) script.

**Conexión local:**
- User: learning_python
- Pass: uY%899892--LL

## Python connector

Use connector "mysql-connector-python" to connect to mysql database from Python. Setup host, user, pass and database name to manage.

- Fetchall, show all rows.
- Fetchone, show one row.
- Insert, update.
- Delete
- Limit