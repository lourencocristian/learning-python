import mysql.connector

# setup connector
mydb = mysql.connector.connect(
    host="localhost",
    user="learning_python",
    password="uY%899892--LL",
    database="hello_mysql"
)

# select rows from table
cursor = mydb.cursor()

cursor.execute('select * from user limit 1')

resp = cursor.fetchall()
print(resp)

# show table definition
#cursor.execute('show create table user')
definition = cursor.fetchall()
print(definition)

# insert rows
sql = 'insert into user (id, email) values (%s, %s)' 
#values = (3,'learning-python@gmail.com')
#cursor.execute(sql, values)
#mydb.commit()

# print(cursor.rowcount)

# update rows
#sql = 'update user set email = %s where id = %s'
#values = ('learning_python@gmail.com', 3)

#cursor.execute(sql, values)
#mydb.commit()

#print(cursor.rowcount)

# delete rows
#sql = 'delete from user where id = %s'
#values = (3,)

#cursor.execute(sql, values)
#mydb.commit()

#print(cursor.rowcount)
